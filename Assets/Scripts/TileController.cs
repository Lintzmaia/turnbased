﻿using UnityEngine;
using System.Collections;

public class TileController : MonoBehaviour {

	ListOfTiles TileList;


	public Material ReachbleGround;
	public Material UnreachbleGround;
	public Material AtackTile;
	public Material HealingTile;
	public Material Neutral;

	public LayerMask myLayer;

	public string TileType;	
	void Awake(){
		TileType = "Neutral";
	}
	void Start(){
		TileList = (ListOfTiles)FindObjectOfType<ListOfTiles> ();
	}
	public string CheckInteractible(){
		if (TileType != "Neutral") {
			return TileType;
		} else {
			return "Neutral";
		}
	}
	public void GroundCheck(string RayName){
		switch (RayName) {
		case "Reach":
			GetComponent<Renderer>().material = ReachbleGround;
			TileType = RayName;
			if(!TileList.ReachbleTiles.Contains(gameObject)){
				TileList.ReachbleTiles.Add(gameObject);
			}
			break;
		case "Unreach":
			GetComponent<Renderer>().material = UnreachbleGround;
			TileType = RayName;
			if(!TileList.UnreachbleTiles.Contains(gameObject)){
				TileList.UnreachbleTiles.Add(gameObject);
			}
			break;
		case "Atack":
			GetComponent<Renderer>().material = AtackTile;
			if(!TileList.AtackTiles.Contains(gameObject)){
				TileList.AtackTiles.Add(gameObject);
			}
			break;

		case "Heal":
			GetComponent<Renderer>().material = HealingTile;
			if(!TileList.AtackTiles.Contains(gameObject)){
				TileList.HealTiles.Add(gameObject);
			}
			gameObject.layer = 11;
			break;
		case "Neutral":
				GetComponent<Renderer>().material = Neutral;
				TileType = RayName;
			break;
		case "TurnAtackOff":
			GroundCheck(TileType);
			gameObject.layer = 8;
			break;
		}
	}


}
