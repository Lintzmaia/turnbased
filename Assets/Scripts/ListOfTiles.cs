﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ListOfTiles : MonoBehaviour {

	public static ListOfTiles List;

	ObjectHolder ControllerHolder;

	public List<GameObject> ReachbleTiles = new List<GameObject>();
	public List<GameObject> AtackTiles = new List<GameObject>();
	public List<GameObject> HealTiles = new List<GameObject>();
	public List<GameObject> UnreachbleTiles = new List<GameObject>();

	public List<GameObject> PartyMenber = new List<GameObject> ();

	public List<GameObject> Enemys = new List<GameObject> ();	

	void Start () {
		if (List == null) {
			GameObject.DontDestroyOnLoad (gameObject);
			List = this;
		} else if (List != this) {
			Destroy(gameObject);
		}


		StartCoroutine (Sort ());
		ControllerHolder = (ObjectHolder)FindObjectOfType<ObjectHolder> ();



	}

	IEnumerator Sort(){
		yield return new WaitForSeconds (0.1f);
		List<int> ToBeSorted = new List<int> ();
		ToBeSorted.Clear ();
		foreach (GameObject O in PartyMenber) {
			ToBeSorted.Add (O.GetComponent<DataHolder> ().Speed);
		}
		ToBeSorted.Sort ();
		List<GameObject> Temp = new List<GameObject> ();
		Temp.Clear ();

		foreach (int I in ToBeSorted) {
			foreach (GameObject O in PartyMenber) {
				if (O.GetComponent<DataHolder> ().Speed == I) {
					if (!Temp.Contains (O)) {
						Temp.Add (O);

					}
				}
			}
		}

		Temp.Reverse ();
		PartyMenber = Temp;

		ToBeSorted = new List<int> ();

		foreach (GameObject O in Enemys) {
			ToBeSorted.Add (O.GetComponent<EnemyPrivateController> ().Speed);
		}
		ToBeSorted.Sort ();
		Temp = new List<GameObject> ();

		foreach (int I in ToBeSorted) {
			foreach (GameObject O in Enemys) {
				if (O.GetComponent<EnemyPrivateController> ().Speed == I) {
					if (!Temp.Contains (O)) {
						Temp.Add (O);
					}
				}
			}
		}
		Temp.Reverse ();
		Enemys.Clear ();
		Enemys = Temp;

		ControllerHolder.SetUpTurn ();


	}


	void OnGUI(){
		GUIStyle myEnemyStyle = new GUIStyle();
		myEnemyStyle.normal.textColor = Color.yellow;
		Color[] EnemyColor = new Color[1];
		EnemyColor [0] = new Vector4(1f, 0f, 0f, 0.75f);
		Texture2D myEnemyTexture = new Texture2D (1, 1);
		myEnemyTexture.SetPixels (EnemyColor);
		myEnemyStyle.normal.background = myEnemyTexture;


		GUIStyle myPlayerStyle = new GUIStyle ();
		myEnemyStyle.normal.textColor = Color.black;
		Color[] PlayerColor = new Color[1];
		PlayerColor [0] = new Vector4 (1, 1, 1, 0.9f);
		Texture2D myPlayerTextuere = new Texture2D (1, 1);
		myPlayerTextuere.SetPixels (PlayerColor);
		myPlayerStyle.normal.background = myPlayerTextuere;
		




		int Pass = 0;



		GUI.Box(new Rect(10, 10, 100, Screen.height - 200),"Enemys List: ", myEnemyStyle);
		foreach (GameObject O in Enemys) {
			EnemyPrivateController Controller = (EnemyPrivateController)O.GetComponent<EnemyPrivateController>();

			GUI.Label(new Rect(15, 40 + Pass * 110, 90, 110),
			          "HP : " +Controller.Health 
			          +"\nDamage: " +Controller.Damage
			          +"\nDefemce: " +Controller.Defense
			          +"\nSpeed: " +Controller.Speed 
			          +"\nRadius: " +Controller.Radius, myEnemyStyle);
				
			Pass++;
		
		}

		Pass = 0;
		GUI.Box(new Rect(Screen.width - 10 - 100, 10, 100, Screen.height - 200),"Party List: ", myPlayerStyle);
		foreach (GameObject O in PartyMenber) {
			DataHolder PartyData = (DataHolder)O.GetComponent<DataHolder>();
			GUI.Label(new Rect(Screen.width - 110 + 5, 40 + Pass * 110, 90, 90), 
			          O.name
			          +"\nHP : " +PartyData.Health 
			          +"\nDamage: " +PartyData.Damage
			          +"\nDefemce: " +PartyData.Defence
			          +"\nSpeed: " +PartyData.Speed, myPlayerStyle);
			
			Pass++;
			
		}
	}
}
