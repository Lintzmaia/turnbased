﻿using UnityEngine;
using System.Collections;

public class ChangeMaterial : MonoBehaviour {
	
	public LayerMask myLayer;
	public string GroundPass;

	RaycastHit Hit;

	void Start(){
		Ray[] CheckRay = new Ray[5];
		CheckRay[0] = new Ray(transform.position + new Vector3(0, 3, 0), new Vector3(0, -1, 0));
		CheckRay[1] = new Ray(transform.position + new Vector3(0.3f, 3, 0), new Vector3(0, -1, 0));
		CheckRay[2] = new Ray(transform.position + new Vector3(-0.3f, 3, 0), new Vector3(0, -1, 0));
		CheckRay[3] = new Ray(transform.position + new Vector3(0, 3, 0.3f), new Vector3(0, -1, 0));
		CheckRay[4] = new Ray(transform.position + new Vector3(0, 3, -0.3f), new Vector3(0, -1, 0));
		for (int pass = 0; pass<CheckRay.Length; pass++) {

			if (Physics.Raycast (CheckRay[pass], out Hit, 10,myLayer)) {
				Hit.collider.gameObject.GetComponent<TileController>().GroundCheck(GroundPass);
				pass = CheckRay.Length + 1;

			}
		}
		Destroy(gameObject);
	}
}
