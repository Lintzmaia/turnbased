﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour {
	Camera Cam;
	// Use this for initialization
	void Start () {
		Cam = GetComponent<Camera> ();
	}

	// Update is called once per frame
	void Update () {	
		if (Input.mouseScrollDelta.y > 0 && Cam.orthographicSize > 1) {
			Cam.orthographicSize -= Input.mouseScrollDelta.y;
		}
		if (Input.mouseScrollDelta.y < 0) {
			Cam.orthographicSize -= Input.mouseScrollDelta.y;
		}

		if (Input.GetMouseButton (2)) {
			float HAxis = -Input.GetAxis("Mouse X");
			float Vaxis = -Input.GetAxis("Mouse Y");
			Vector3 Target = new Vector3(HAxis + Vaxis, 0, Vaxis - HAxis)  + transform.position;
			transform.position = Vector3.MoveTowards(transform.position, Target, 1);
		}
	}
}
