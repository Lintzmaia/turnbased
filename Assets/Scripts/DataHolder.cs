﻿using UnityEngine;
using System.Collections;

public class DataHolder : MonoBehaviour {
	public int Health;
	public int Defence;
	public int Damage;
	public int Speed;

	int MaxHealth;
	ListOfTiles TileList;

	void Start(){
		MaxHealth = Health;
		TileList = (ListOfTiles)FindObjectOfType<ListOfTiles> ();
		ReturnNeutralStateAfterTurn = false;
	}


	public void TakeDamage(int HitDamage){
		if (HitDamage - Defence < 0) {
			Health--;
		} else {
			Health -= HitDamage - Defence;
		}
		if (Health <= 0) {
			TileList.PartyMenber.Remove(gameObject);
			gameObject.GetComponent<CapsuleCollider>().enabled = false;

		}	
	}

	public void BeHealed(int HealFactor){
		Health += HealFactor;
		if (Health > MaxHealth) {
			Health = MaxHealth;
		}

	}

	bool StateHolder;
	bool ReturnNeutralStateAfterTurn;
	public void OnHealTile(bool NeutralState){
		GetComponent<CapsuleCollider> ().enabled = true;
		StateHolder = NeutralState;
		ReturnNeutralStateAfterTurn = true;
		gameObject.layer = 10;
	}
	public void ResetCollider(){
		if (ReturnNeutralStateAfterTurn) {
			GetComponent<CapsuleCollider>().enabled = StateHolder;
			ReturnNeutralStateAfterTurn = false;
			gameObject.layer = 9;
		}
	}

}
