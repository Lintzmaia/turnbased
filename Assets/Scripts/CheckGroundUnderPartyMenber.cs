﻿using UnityEngine;
using System.Collections;

public class CheckGroundUnderPartyMenber : MonoBehaviour {

	ListOfTiles TileList;
	public LayerMask myGroundLayer;
	public LayerMask myHealingLayer;
	bool RunCheck;

	void Start () {
		TileList = (ListOfTiles)FindObjectOfType<ListOfTiles> ();
	}
	

	void Update () {
		if (RunCheck) {
			Debug.Log("RUN CHeck");
			RaycastHit Hit;
			Ray Check = new Ray (transform.position, new Vector3 (0, -1, 0));
			if (Physics.Raycast (Check, out Hit, myGroundLayer, myGroundLayer)) {
				if (TileList.ReachbleTiles.Contains (Hit.collider.gameObject)) {
					TileList.ReachbleTiles.Remove (Hit.collider.gameObject);
					Hit.collider.GetComponent<TileController> ().GroundCheck ("Unreach");
					RunCheck = false;
				}
			}
			Check = new Ray (transform.position, new Vector3 (0, -1, 0));
			if (Physics.Raycast (Check, out Hit, myGroundLayer, myHealingLayer)) {
				if(TileList.HealTiles.Contains(Hit.collider.gameObject)){
					GetComponent<DataHolder>().OnHealTile(GetComponent<CapsuleCollider>().enabled);
					RunCheck = false;
				}
			}
		}
	}


	
	IEnumerator CleanData(){
		yield return new WaitForSeconds(0.02f);
		RunCheck = false;
	}

	public void Check(){
		RunCheck = true;
		StartCoroutine (CleanData ());
	}
}
