﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectHolder : MonoBehaviour {
	public List<GameObject> TempPartyMenber;
	public List<GameObject> TempEnemyList;

	public static ObjectHolder HolderController;


	ListOfTiles TileList;
	AIManeger AI;
	CheckGroundUnderPartyMenber[] PartyMenber;


	public GameObject[] Button;

	public Material CloneMaterial;

	public LayerMask myLayer;

	public LayerMask myInteractbleLayer;

	CreateRadiusRay CreateRadius;


	bool ShowingAtackRadius;
	public GameObject Holder;
	Camera Can; 
	
	GameObject Clone;

	public bool ForceToAtack;

	void Awake(){
		if (HolderController == null) {
			GameObject.DontDestroyOnLoad (gameObject);
			HolderController = this;
		} else if (HolderController != this) {
			Destroy(gameObject);
		}

		foreach (GameObject B in Button) {
			B.SetActive(false);
		}
		
	}


	void Start () {
		PartyMenber = FindObjectsOfType<CheckGroundUnderPartyMenber> ();

		TileList = (ListOfTiles)FindObjectOfType<ListOfTiles> ();

		AI = (AIManeger)FindObjectOfType<AIManeger> ();

		Can = GetComponent<Camera> ();
		SetUpTurn ();
	}
	bool HitBottun;
	void Update () {
		Ray Check = Can.ScreenPointToRay (Input.mousePosition);
		RaycastHit Hit;
		if (Input.GetMouseButtonDown (0) && !ForceToAtack) {
			if (Physics.Raycast (Check, out Hit, Mathf.Infinity, myLayer)) {
				if (Hit.collider.gameObject.layer == 9) {
					if (Holder == null) {
						Holder = Hit.collider.gameObject;
						CreateRadius = (CreateRadiusRay)Holder.GetComponent<CreateRadiusRay> ();
						Button [0].SetActive (true);
						Button [1].SetActive (true);
						Button [2].SetActive (true);
						CreateRadius.CreateMovementRadius ();
						foreach (GameObject O in TempEnemyList) {
							O.GetComponent<EnemyPrivateController> ().Switch (true);
						}
						foreach (CheckGroundUnderPartyMenber O in PartyMenber) {
							O.Check ();
						
						}

					} else if (Holder == Hit.collider.gameObject) {
						HighlighhtAtackTiles ();
					} else if (Hit.collider.gameObject == Clone) {
						HighlighhtAtackTiles ();
					} else {

						TurnMovementTilesOff ();


						if (Clone != null) {
							StopAllCoroutines ();
							GameObject.Destroy (Clone);
							Clone = null;
						}
						Holder = Hit.collider.gameObject;
						CreateRadius = (CreateRadiusRay)Holder.GetComponent<CreateRadiusRay> ();
						CreateRadius.CreateMovementRadius ();


						foreach (GameObject O in TempEnemyList) {
							O.GetComponent<EnemyPrivateController> ().Switch (true);
						}
						foreach (CheckGroundUnderPartyMenber O in PartyMenber) {
							O.Check ();
							
						}

					}
				} else if (Holder != null) {
					switch (Hit.collider.gameObject.GetComponent<TileController> ().CheckInteractible ()) {			
					case "Reach":
						MoveClonePartyMenber (Hit.collider.transform.position);
						AtackRadiusOff ();
						TileList.AtackTiles.Clear ();
						break;
					default:
						break;
					}

				}
			}
		} else if (Input.GetMouseButtonDown (0)){
			if(Physics.Raycast (Check, out Hit, myInteractbleLayer, myInteractbleLayer)) {
				AtackInteractble(Hit.collider.gameObject);
			}
		}


		if (SetEndTurnPos) {
			Holder.transform.position = Vector3.MoveTowards(Holder.transform.position, CloneEndPos,0.25f);
			if(Holder.transform.position == CloneEndPos){
				HighlighhtAtackTiles();
				Button [3].SetActive (true);
				if(Holder.GetComponent<CreateRadiusRay>().Healer){
					HealParty();
				}
				SetEndTurnPos = false;
			}

		}
	}

	public void HealParty(){
		foreach (GameObject O in TileList.PartyMenber) {
			if(O != Holder){
				O.GetComponent<CheckGroundUnderPartyMenber>().Check();
			}
		}
	}


	public void TurnMovementTilesOff(){
		AtackRadiusOff();
		foreach(GameObject O in TileList.UnreachbleTiles){
			O.GetComponent<TileController>().GroundCheck("Neutral");
		}
		foreach(GameObject O in TileList.ReachbleTiles){
			O.GetComponent<TileController>().GroundCheck("Neutral");
		}
		TileList.UnreachbleTiles.Clear();
		TileList.ReachbleTiles.Clear();
		TileList.AtackTiles.Clear();
		ShowingAtackRadius = false;	
	}


	public void DeselectPartyMenber(){

		TurnMovementTilesOff();
		
		
		if (Clone != null){
			Holder.GetComponent<CreateRadiusRay>().GetClone(null, false);
			StopAllCoroutines();
			Destroy(Clone.gameObject);
			Clone = null;
		}
		foreach (GameObject B in Button) {
			B.SetActive(false);
		}
		
		Holder = null;
	
	}

	bool SetEndTurnPos;
	Vector3 CloneEndPos;

	public void EndMovemtTurn(){

		if (ShowingAtackRadius) {
			AtackRadiusOff();
		}
		SetEndTurnPos = true;
		TurnMovementTilesOff ();	
		ForceToAtack = true;
		if (Clone != null) {
			CloneEndPos = Clone.transform.position;
			Destroy (Clone);
			Clone = null;
		} else {
			CloneEndPos = Holder.transform.position;
		}	
		Button [0].SetActive (false);
		Button [1].SetActive (false);
		Button [2].SetActive (false);
	}

	public void AtackInteractble(GameObject Interactble){
		Holder.GetComponent<CapsuleCollider> ().enabled = false;
		MoveCount--;
		if (Interactble.tag != "MainCamera") {
			if(Interactble.tag == "Enemy"){
				Interactble.GetComponent<EnemyPrivateController>().TakeDamage(Holder.GetComponent<DataHolder>().Damage);
			}else{
				Interactble.GetComponent<DataHolder>().BeHealed(Holder.GetComponent<DataHolder>().Damage);
			}
		}

		Button [3].SetActive (false);
		AtackRadiusOff();
		if (MoveCount <= 0) {
			if(TempEnemyList.Count > 0){
				AI.TurnManeger();
			}else{
				SetUpTurn();
			}


		} else {
			ForceToAtack = false;
			TileList.AtackTiles.Clear();
		}


		foreach (GameObject O in TempEnemyList) {
			O.GetComponent<EnemyPrivateController>().Switch(false);
		}
		foreach (GameObject O in TempPartyMenber) {
			O.GetComponent<DataHolder>().ResetCollider();
		}

		Holder = null;
	}

	public int MoveCount;

	public void SetUpTurn(){
		TileList.AtackTiles.Clear();
		ForceToAtack = false;
		TempEnemyList = new List<GameObject> ();
		TempEnemyList = TileList.Enemys;
		TempPartyMenber = new List<GameObject> ();
		TempPartyMenber = TileList.PartyMenber;
		MoveCount = TempPartyMenber.Count;
		foreach (GameObject O in TempPartyMenber) {
			O.GetComponent<CapsuleCollider>().enabled = true;
		}
	}


	public void HighlighhtAtackTiles(){
		if (Clone != null) {
			CreateRadius.GetClone (Clone, true);
		} else {
			CreateRadius.GetClone (null, false);
		}
		if(!ShowingAtackRadius){
			CreateRadius.CreateAtackRadius ();
			foreach (GameObject O in TempEnemyList) {
				O.GetComponent<EnemyPrivateController>().Switch(true);
			}
			ShowingAtackRadius = true;
		}else {

			AtackRadiusOff();
		}
	}

	public void MoveClonePartyMenber(Vector3 Pos){
		if (Clone == null) {
			Clone = Instantiate (Holder, Holder.transform.position, Quaternion.identity) as GameObject;
			Clone.GetComponent<Renderer> ().material = CloneMaterial;
			Clone.GetComponent<CreateRadiusRay>().enabled = false;
			Clone.transform.SetParent(Holder.transform);
		}
		Moving = true;
		StopAllCoroutines ();

		StartCoroutine (MovementUpdate (Clone.gameObject, Pos));
		
	}
	bool Moving;


	IEnumerator MovementUpdate(GameObject Clone, Vector3 Pos){
		while (Moving) {
			Clone.transform.position = Vector3.MoveTowards (Clone.transform.position, 
			                                               new Vector3 (Pos.x, Clone.transform.position.y, Pos.z), 1);
			if (Clone.transform.position == new Vector3(Pos.x, Clone.transform.position.y, Pos.z)){
				Moving = false;

			}
			yield return new WaitForSeconds(0.1f);
		}
	}
	public void AtackRadiusOff(){
		foreach (GameObject O in TileList.AtackTiles) {
			O.gameObject.GetComponent<TileController>().GroundCheck ("TurnAtackOff");
		}
		foreach (GameObject O in TileList.HealTiles) {
			O.gameObject.GetComponent<TileController>().GroundCheck("TurnAtackOff");
		}
		foreach (GameObject O in TempEnemyList) {
			O.GetComponent<EnemyPrivateController>().Switch(false);
		}
		ShowingAtackRadius = false;
	}


}
