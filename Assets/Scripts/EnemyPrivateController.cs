﻿using UnityEngine;
using System.Collections;

public class EnemyPrivateController : MonoBehaviour {

	public int Health;
	public int Defense;

	public int Damage;
	public int Speed;

	public int Radius;
	public int AtackRadius;


	public Material CanTakeDamege;
	Material NeutralMaterial;
	ListOfTiles TileList;

	public LayerMask myGroundLayer;
	BoxCollider Self;
	
	void OnGui(){
		GUI.Label (new Rect(), Health.ToString());
	
	}

	void Start () {
		Self = GetComponent<BoxCollider> ();
		Self.enabled = false;
		NeutralMaterial = GetComponent<Renderer> ().material;
		TileList = (ListOfTiles)FindObjectOfType<ListOfTiles> ();
		AddToList ();
	}

	public void AddToList(){
		TileList.Enemys.Add (gameObject);
	}

	void Update(){
		if (RunCheck) {
			RaycastHit Hit;
			Ray Check = new Ray (transform.position, new Vector3 (0, -1, 0));
			if (Physics.Raycast (Check, out Hit, myGroundLayer, myGroundLayer)) {
				if (TileList.AtackTiles.Contains (Hit.collider.gameObject)) {
					GetComponent<Renderer> ().material = CanTakeDamege;
					Self.enabled = true;
					RunCheck = false;
				}else if(TileList.ReachbleTiles.Contains(Hit.collider.gameObject)){
					TileList.ReachbleTiles.Remove(Hit.collider.gameObject);
					Hit.collider.GetComponent<TileController>().GroundCheck("Unreach");
				}
			}
		}




	}

	public void TakeDamage(int HitDamage){
		Health -= HitDamage - Defense;
		if (Health <= 0) {
			TileList.Enemys.Remove(gameObject);
			Destroy(gameObject);
		}
	
	}


	bool RunCheck;
	IEnumerator CleanData(){
		yield return new WaitForSeconds(0.3f);
		RunCheck = false;
	}


	public void Switch(bool On_Off){
		switch(On_Off){
		case true:
			StopAllCoroutines();
			StartCoroutine(CleanData());
			RunCheck = true;
			break;
		case false:
			GetComponent<Renderer>().material = NeutralMaterial;
			Self.enabled = false;
			break;
		}
	}
}
