﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Randon = UnityEngine.Random;

public class AIManeger : MonoBehaviour {

	EnemyPrivateController PrivateController;

	ListOfTiles TileList;
	ObjectHolder HolderController;

	public LayerMask myGroundLayer;

	public List<GameObject> TempEnemys;
	public List<GameObject> TempPartyMenbers;


	GameObject Target;
	Vector3 StartPos;

	int Radius;
	int AtackRadius;
	int Damage;

	void Start () {
		TileList = (ListOfTiles)FindObjectOfType <ListOfTiles>();
		HolderController = (ObjectHolder)FindObjectOfType<ObjectHolder> ();
		Pass = 0;
	}

	bool HitPartyMenber;
	bool ReachBorder;
	bool EndCycle;
	RaycastHit Hit;

	GameObject Current;
	int Pass;
	int Count;
	
	void SetUp(int Index){

		EndCycle = false;
		HitPartyMenber = false;
		ReachBorder = false;
		TempPartyMenbers = new List<GameObject> ();
		TempPartyMenbers = TileList.PartyMenber;
		Target = TempPartyMenbers [Randon.Range (0, TempPartyMenbers.Count - 1)];
		Current = TempEnemys [Index];
		StartPos = Current.transform.position;
		PrivateController = (EnemyPrivateController)Current.GetComponent<EnemyPrivateController> ();
		AtackRadius = PrivateController.AtackRadius;
		Radius = PrivateController.Radius;
		Damage = PrivateController.Damage;

		if (Mathf.Round (Vector3.Distance (Current.transform.position, Target.transform.position)) <= AtackRadius) {
			HitPartyMenber = true;
			
		} else if (Radius == 0){
			ReachBorder = true;	

		}else{
			StartCoroutine(MoveEnemy());

		}
	}
	public void TurnManeger(){
		EndCycle = false;
		TempEnemys = new List<GameObject> ();
		TempEnemys = TileList.Enemys;
		Count = TempEnemys.Count;
		Pass = 0;
		SetUp (Pass);
	}


	IEnumerator MoveEnemy(){
		while (true) {
			if (Mathf.Round(Vector3.Distance (Current.transform.position, Target.transform.position)) <= AtackRadius) {
				HitPartyMenber = true;
				EndCycle = true;
				
			} else if(Mathf.Round(Vector3.Distance(Current.transform.position, StartPos)) >= Radius){
				ReachBorder = true;
				EndCycle = true;
				
			} else {
				
				Vector3 Direction = (Target.transform.position - Current.transform.position).normalized;
				Ray Check = new Ray (Current.transform.position + Direction + new Vector3 (0, 2, 0),
				                     new Vector3 (0, -1, 0));
				if (Physics.Raycast (Check, out Hit, myGroundLayer, myGroundLayer)) {
					Vector3 NextPos = new Vector3(Hit.collider.transform.position.x, Current.transform.position.y,
					                              Hit.collider.transform.position.z);
					Current.transform.position = Vector3.MoveTowards (Current.transform.position,
					                                                  NextPos,
					                                                  1f);
				}
			}

			yield return new WaitForSeconds (0.1f);
		}
	}

	void EndMove(string Choice){

		EndCycle = true;
		if (Choice == "Atack") {
			Target.GetComponent<DataHolder>().TakeDamage(Damage);
		}
		Pass++;
		if (Pass < Count) {
			SetUp (Pass);
		} else {
			HolderController.enabled = true;
			HolderController.SetUpTurn();
		}
	}

	void Update () {
		if (EndCycle) {
			StopAllCoroutines();
				
		}if(ReachBorder){
			if(Physics.Raycast(Current.transform.position + new Vector3(0, 2, 0), new Vector3(0, -1, 0),
			                  out Hit, myGroundLayer, myGroundLayer)){
				Current.transform.position = new Vector3(Hit.collider.transform.position.x, Current.transform.position.y,
				                                 Hit.collider.transform.position.z);
			}

			if (Mathf.Round(Vector3.Distance (Current.transform.position, Target.transform.position)) <= AtackRadius) {
				HitPartyMenber = true;
				
			}else {
				EndMove("Null");
				ReachBorder = false;
			}
		}


		if (HitPartyMenber) {

		HitPartyMenber = false;
			EndMove("Atack");
		}
	}
}
