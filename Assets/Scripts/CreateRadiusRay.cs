﻿using UnityEngine;
using System.Collections;

public class CreateRadiusRay : MonoBehaviour {
	public GameObject ReachbleGround;
	public GameObject UnreachbleGround;
	public GameObject AtackingTile;
	public GameObject NeutralGround;
	public GameObject HelaingTile;

	public bool Healer;

	public float Radius;
	public float AtackRadius;
	public int NunofPoints = 5;
	public int AtackNunofPoints = 4;


	public void CreateMovementRadius(){
		for (float pass = 0; pass < Radius + 2; pass++) {
			for (int pointNum = 0; pointNum < NunofPoints * pass + 1; pointNum++) {

				float i = (pointNum * 1.0f) / NunofPoints;
				float Angle = i * Mathf.PI * 2;
				float x = Mathf.Sin (Angle) * pass;
				float y = Mathf.Cos (Angle) * pass;
				Vector3 Pos = new Vector3 (x, 0, y) + transform.position;
				if(pass < Radius){
					GameObject.Instantiate (ReachbleGround, Pos, Quaternion.identity);
				}else {
					GameObject.Instantiate (UnreachbleGround, Pos, Quaternion.identity);	
				}
			}
		}
	}

	public bool HasClone;
	GameObject Shadow;

	public void GetClone(GameObject Clone, bool Value){
		Shadow = Clone;
		HasClone = Value;
	}

	public void CreateAtackRadius () {

		for (float pass = 0; pass < AtackRadius; pass++) {
			for (int pointNum = 0; pointNum < AtackNunofPoints; pointNum++) {
			
				float i = (pointNum * 1.0f) / AtackNunofPoints;
				float Angle = i * Mathf.PI * 2;
				float x = Mathf.Sin (Angle) * pass;
				float y = Mathf.Cos (Angle) * pass;
				Vector3 Pos = Vector3.zero;
				if(!HasClone){
					Pos = new Vector3 (x, 0, y) + transform.position;
				}else {
					Pos = new Vector3 (x, 0, y) + Shadow.transform.position;
				}
				if(!Healer){
					GameObject.Instantiate (AtackingTile, Pos, Quaternion.identity);
				}else{
					GameObject.Instantiate(HelaingTile, Pos, Quaternion.identity);
				}

			}
		}
		


	}
}
