﻿using UnityEngine;
using System.Collections;

public class AddPartyMenber : MonoBehaviour {

	ListOfTiles TileList;

	void Start() {
		TileList = (ListOfTiles)FindObjectOfType<ListOfTiles> ();
		AddToList ();
	}
	public void AddToList(){
		TileList.PartyMenber.Add (gameObject);
		this.enabled = false;
	}
}
